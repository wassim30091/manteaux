/*
 * Copyright (C) 2014 Antonio Leiva Gordillo.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wwsight.manteaux;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluejamesbond.text.DocumentView;

import java.util.List;

public class DetailActivity extends BaseActivity {

    public static final String EXTRA_IMAGE = "DetailActivity:image";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final List<Product> cart = ShoppingCartHelper.getCart();
        final List<String> elementInCart = ShoppingCartHelper.getElementInCart();

        Bundle extras = getIntent().getExtras();

        String path = null;
        Product objProduct = new Product();
        if (extras != null) {
            objProduct = (Product)getIntent().getSerializableExtra("Product");
        }

        ImageView image = (ImageView) findViewById(R.id.image);
        int idImage = getResources().getIdentifier("com.wwsight.manteaux:drawable/" + objProduct.getPath().split("\\.")[0], null, null);
        image.setImageResource(idImage);

        DocumentView description = (DocumentView) findViewById(R.id.description);
        description.setText(objProduct.getDescription());

        TextView price = (TextView) findViewById(R.id.price);
        price.setText(objProduct.getPrice() + " $");

        TextView color = (TextView) findViewById(R.id.color);
        color.setText(objProduct.getColor());

        TextView manufacturer = (TextView) findViewById(R.id.manufacturer);
        manufacturer.setText(objProduct.getManufacturer());

        //ImageView productImageView = (ImageView) findViewById(R.id.ImageViewProduct);
        //ViewCompat.setTransitionName(image, EXTRA_IMAGE);
        //Picasso.with(this).load(getIntent().getStringExtra(EXTRA_IMAGE)).into(image);
        //buton add to cart
        final Button addToCartButton = (Button) findViewById(R.id.ButtonAddToCart);

        final Product finalObjProduct = objProduct;
        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Disable the add to cart button if the item is already in the cart
                if(!elementInCart.contains(finalObjProduct.getIdReference())) {
                    elementInCart.add(finalObjProduct.getIdReference());
                    cart.add(finalObjProduct);
                    ShoppingCartHelper.nbrProduct++;
                    ShoppingCartHelper.totalPrice += Integer.valueOf(finalObjProduct.getPrice());
                    finish();
                }else {
                    addToCartButton.setEnabled(false);
                    addToCartButton.setText("Item in Cart");
                }
            }
        });
    }

    @Override protected int getLayoutResource() {
        return R.layout.activity_detail;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        if(ShoppingCartHelper.nbrProduct > 0) {
            MenuItem item = menu.findItem(R.id.shopping_cart);
            item.setIcon(R.drawable.ic_cart_full);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.shopping_cart && ShoppingCartHelper.nbrProduct > 0) {
            Intent viewShoppingCartIntent = new Intent(getBaseContext(), ShoppingCartActivity.class);
            startActivity(viewShoppingCartIntent);
        }else{
            new AlertDialog.Builder(DetailActivity.this)
                    .setTitle("Your basket is empty")
                    .setMessage("you do not have any items in cart")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        //do nothing
                        public void onClick(DialogInterface dialog, int which) {
                            //do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    /*public static void launch(BaseActivity activity, View transitionView, String url) {
        ActivityOptionsCompat options =
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, transitionView, EXTRA_IMAGE);
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(EXTRA_IMAGE, url);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }*/
}
