package com.wwsight.manteaux;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;


public class paymentActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.payment);
        Button bigBankButton = (Button) findViewById(R.id.bigbank);
        bigBankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton bigradio = (RadioButton) findViewById(R.id.bigbankRadio);
                RadioButton cbradio = (RadioButton) findViewById(R.id.carteBancaireRadio);
                if (bigradio.isChecked()) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.setComponent(new ComponentName("com.wwsight.bigbank", "com.wwsight.bigbank.Activity.TapcardActivity"));
                    intent.putExtra("nameApp", "Manteaux");
                    intent.putExtra("price", String.valueOf(ShoppingCartHelper.totalPrice));
                    intent.putExtra("comments", "Clothes for man and woman");

                    //convertir le panier en string pour l'afficher dans l'application BigBank
                    String[] products = new String[ShoppingCartHelper.getCart().size()];
                    int index = 0;
                    for (Product value : ShoppingCartHelper.getCart()) {
                        products[index] = value.getTitle() + "//" + value.getPrice();
                        index++;
                    }
                    intent.putExtra("products", (Serializable) products);
                    boolean installed = appInstalledOrNot("com.wwsight.bigbank");
                    if (installed) {
                        startActivity(intent);
                    } else {
                        new AlertDialog.Builder(paymentActivity.this)
                                .setTitle("Bigbank is not installed")
                                .setMessage("Do you want to install the Application ?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=bigbank")));
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                }else if(cbradio.isChecked()){
                    new AlertDialog.Builder(paymentActivity.this)
                            .setTitle("Disabled Credit card")
                            .setMessage("This payment mode is disabled")
                            .setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }else{
                    new AlertDialog.Builder(paymentActivity.this)
                            .setTitle("Choose a mode of payment")
                            .setMessage("No payment mode is chosen")
                            .setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }
        });
    }

    @Override protected int getLayoutResource() {
        return R.layout.payment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.shopping_cart && ShoppingCartHelper.nbrProduct > 0) {
            Intent viewShoppingCartIntent = new Intent(getBaseContext(), ShoppingCartActivity.class);
            startActivity(viewShoppingCartIntent);
        }else{
            new AlertDialog.Builder(paymentActivity.this)
                    .setTitle("Your basket is empty")
                    .setMessage("you do not have any items in cart")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        //do nothing
                        public void onClick(DialogInterface dialog, int which) {
                            //do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        if(ShoppingCartHelper.nbrProduct > 0) {
            MenuItem item = menu.findItem(R.id.shopping_cart);
            item.setIcon(R.drawable.ic_cart_full);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getBaseContext(), ShoppingCartActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}
