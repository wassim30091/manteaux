package com.wwsight.manteaux;

import android.content.res.Resources;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

/**
 * Created by labiadh on 22/06/2015.
 */
public class ShoppingCartHelper implements Serializable {

    public static final String PRODUCT_INDEX = "PRODUCT_INDEX";
    public static Integer totalPrice = 0;
    public static  int nbrProduct;

    public static List<Product> cart;
    public static List<String> elementInCart;

    public static List<Product> getCart() {
        if(cart == null) {
            cart = new Vector<Product>();
        }
        return cart;
    }

    public static List<String> getElementInCart() {
        if(elementInCart == null) {
            elementInCart = new Vector<String>();
        }
        return elementInCart;
    }
}
