package com.wwsight.manteaux;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by labiadh on 22/06/2015.
 */
public class ShoppingCartActivity extends BaseActivity {
    private List<Product> mCartList;
    private List<String> mProductCartList;
    private ProductAdapter mProductAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.shoppingcart);

        mCartList = ShoppingCartHelper.getCart();
        mProductCartList = ShoppingCartHelper.getElementInCart();

        // Create the list
        final ListView listViewCatalog = (ListView) findViewById(R.id.ListViewCatalog);
        mProductAdapter = new ProductAdapter(mCartList, getLayoutInflater(), true);
        listViewCatalog.setAdapter(mProductAdapter);

        listViewCatalog.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                Product selectedProduct = mCartList.get(position);
                if (selectedProduct.selected == true)
                    selectedProduct.selected = false;
                else
                    selectedProduct.selected = true;

                mProductAdapter.notifyDataSetInvalidated();

            }
        });

        final TextView totalPrice = (TextView) findViewById(R.id.totalPrice);
        totalPrice.setText("Total : " + String.valueOf(ShoppingCartHelper.totalPrice) + " $");

        Button removeButton = (Button) findViewById(R.id.ButtonRemoveFromCart);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Loop through and remove all the products that are selected
                // Loop backwards so that the remove works correctly
                for (int i = mCartList.size() - 1; i >= 0; i--) {
                    if (mCartList.get(i).selected) {
                        ShoppingCartHelper.totalPrice -= Integer.valueOf(mCartList.get(i).getPrice());
                        mCartList.remove(i);
                        mProductCartList.remove(i);
                        ShoppingCartHelper.nbrProduct--;
                        totalPrice.setText("Total : " + String.valueOf(ShoppingCartHelper.totalPrice) + " $");
                        if (ShoppingCartHelper.nbrProduct == 0) {
                            finish();
                        }
                    }
                }
                mProductAdapter.notifyDataSetChanged();
            }
        });

        Button paymentButton = (Button) findViewById(R.id.ButtonPayment);
        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), paymentActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override protected int getLayoutResource() {
        return R.layout.shoppingcart;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if(ShoppingCartHelper.nbrProduct > 0) {
            MenuItem item = menu.findItem(R.id.shopping_cart);
            item.setIcon(R.drawable.ic_cart_full);
            /*View target = findViewById(R.id.shopping_cart);
            BadgeView badge = new BadgeView(this, target);
            badge.setText(String.valueOf(ShoppingCartHelper.nbrProduct));
            badge.show();*/
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.shopping_cart) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
