package com.wwsight.manteaux;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class HomeActivity extends BaseActivity {

    private DrawerLayout drawer;
    ArrayList<Product> mProducts = null;
    private Menu myMenu = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent myIntent = getIntent();
        if(myIntent.getStringExtra("payement") != null){
            //initialiser le panier
            ShoppingCartHelper.totalPrice = 0;
            ShoppingCartHelper.nbrProduct = 0;
            ShoppingCartHelper.cart = null;
            ShoppingCartHelper.elementInCart = null;
        }

        setActionBarIcon(R.drawable.ic_ab_drawer);

        System.out.println("TEST  : Launching activity Home");

        //----- Parser le XML -------
        XmlPullParserFactory pullParserFactory;
        try {
            pullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = pullParserFactory.newPullParser();

            InputStream in_s = getApplicationContext().getAssets().open("input.xml");
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in_s, null);

            mProducts = new ArrayList<>(parseXML(parser));

        } catch (XmlPullParserException e) {

            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //-----------------------------

        GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new GridViewAdapter());
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String url = (String) view.getTag();
                //Create intent
                Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                intent.putExtra("Product",mProducts.get(i));

                //Start details activity
                startActivity(intent);
                //DetailActivity.launch(HomeActivity.this, view.findViewById(R.id.image), url);
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
    }

    /**
     * Parses the xml input file and creates an ArrayList of Products
     */
    private ArrayList<Product> parseXML(XmlPullParser parser) throws XmlPullParserException,IOException
    {
        ArrayList<Product> Products = null;
        int eventType = parser.getEventType();
        Product currentProduct = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String name;
            switch (eventType){
                case XmlPullParser.START_DOCUMENT:
                    Products = new ArrayList();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    if (name.equals("item")){
                        currentProduct = new Product();
                    } else if (currentProduct != null){
                        if (name.equals("idReference")){
                            currentProduct.idReference = parser.nextText();
                        } else if (name.equals("title")){
                            currentProduct.title = parser.nextText();
                        } else if (name.equals("category")){
                            currentProduct.category = parser.nextText();
                        } else if (name.equals("price")){
                            currentProduct.price= parser.nextText();
                        } else if (name.equals("description")){
                            currentProduct.description = parser.nextText();
                        } else if (name.equals("manufacturer")){
                            currentProduct.manufacturer= parser.nextText();
                        } else if (name.equals("color")){
                            currentProduct.color = parser.nextText();
                        } else if (name.equals("path")){
                            currentProduct.path= parser.nextText();
                        }
                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (name.equalsIgnoreCase("item") && currentProduct != null){
                        int idImage = getResources().getIdentifier("com.wwsight.manteaux:drawable/" + currentProduct.path.split("\\.")[0]+"prev", null, null);
                        currentProduct.setIdImage(idImage);
                        Products.add(currentProduct);
                    }
            }
            eventType = parser.next();
        }
        return Products;
    }

    /**
     * Disable dispacher Remove the most important priority for foreground application
     */
    public void onPause() {
        super.onPause();
        Intent myIntent = getIntent();
        if(myIntent.getStringExtra("payment") != null){
            //initialiser le panier
            ShoppingCartHelper.totalPrice = 0;
            ShoppingCartHelper.nbrProduct = 0;
            ShoppingCartHelper.cart = null;
            ShoppingCartHelper.elementInCart = null;
        }
    }

    /**
     * Activate NFC dispacher to read NFC Card Set the most important priority to the foreground application
     */
    protected void onResume() {
        super.onResume() ;

        Intent myIntent = getIntent();
        if(myIntent.getStringExtra("payment") != null){
            //initialiser le panier
            ShoppingCartHelper.totalPrice = 0;
            ShoppingCartHelper.nbrProduct = 0;
            ShoppingCartHelper.cart = null;
            ShoppingCartHelper.elementInCart = null;
        }

        if(myMenu != null) {
            MenuItem item = myMenu.findItem(R.id.shopping_cart);
            if (ShoppingCartHelper.nbrProduct > 0) {
                item.setIcon(R.drawable.ic_cart_full);
            } else {
                item.setIcon(R.drawable.ic_cart);
            }
        }
    }

    @Override protected int getLayoutResource() {
        return R.layout.activity_home;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        myMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(Gravity.START);
                return true;
            case R.id.shopping_cart:
                if(ShoppingCartHelper.nbrProduct > 0) {
                    Intent viewShoppingCartIntent = new Intent(getBaseContext(), ShoppingCartActivity.class);
                    startActivity(viewShoppingCartIntent);
                    return true;
                }else{
                    new AlertDialog.Builder(HomeActivity.this)
                            .setTitle("Your basket is empty")
                            .setMessage("you do not have any items in cart")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                //do nothing
                                public void onClick(DialogInterface dialog, int which) {
                                    //do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }
        }

        return super.onOptionsItemSelected(item);
    }

    private class GridViewAdapter extends BaseAdapter {

        @Override public int getCount() {
            return mProducts.size();
        }

        @Override public Object getItem(int i) {
            return "Item " + String.valueOf(i + 1);
        }

        @Override public long getItemId(int i) {
            return i;
        }

        @Override public View getView(int i, View view, ViewGroup viewGroup) {

            if (view == null) {
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.grid_item, viewGroup, false);
            }

            String imageUrl = mProducts.get(i).getPath();
            view.setTag(imageUrl);

            ImageView image = (ImageView) view.findViewById(R.id.image);
            image.setImageResource(mProducts.get(i).getIdImage());


            TextView text = (TextView) view.findViewById(R.id.text);
            text.setText(mProducts.get(i).getTitle());

            TextView price = (TextView) view.findViewById(R.id.price);
            price.setText(mProducts.get(i).getPrice() + " $");

            return view;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
