package com.wwsight.manteaux;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by ayoub on 18/06/2015.
 */
public class Product implements Serializable{
    public String idReference;
    public String category;
    public String title;
    public String price;
    public String description;
    public String manufacturer;
    public String color;
    public String path;
    public boolean selected = true;
    private int idImage;

/*
    public Product(String _idReference, String _category, String _title, String _price,
                        String _description, String _manufacturer, String _color, String _path){
        this.idReference = _idReference;
        this.category = _category;
        this.title = _title;
        this.price = _price;
        this.description = _description;
        this.manufacturer = _manufacturer;
        this.color = _color;
        this.path = _path;
    }*/

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIdReference() {
        return idReference;
    }

    public void setIdReference(String idReference) {
        this.idReference = idReference;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public int getIdImage() {
        return idImage;
    }
}
